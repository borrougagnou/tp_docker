# TP1

## Installation
J'ai installé CentOS 8,
et pour installer docker, j'ai du faire:

EN ROOT:
```sh
#>
yum update
firewall-cmd --add-masquerade --zone=public --permanent && sudo firewall-cmd --reload
yum install -y yum-utils device-mapper-persistent-data lvm2 curl wget git
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum update
dnf -y install docker-ce --nobest
systemctl enable --now docker
usermod -aG docker anon
curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

je quitte le root
je teste en lançant alpine:
```sh
docker run alpine
docker ps -a
```
il me retourne:
```
CONTAINER ID        IMAGE               COMMAND             CREATED              STATUS                          PORTS               NAMES
64240f28b9e3        alpine              "/bin/sh"           About a minute ago   Exited (0) About a minute ago                       mystifying_kalam
```
le container et docker fonctionnent.


## Lancer des conteneurs
```
$> docker run -d alpine sleep 9999
f07ec4cf1906eac98dd9476e2ff40f8da1486ae10a4c9896db27ab6564360d48
```

En faisant un `docker ps`on voit l'ID du container:f07ec4cf1906 et son nom:infallible_stallman
```
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                         PORTS               NAMES
f07ec4cf1906        alpine              "sleep 9999"        43 seconds ago      Up 42 seconds                                      infallible_stallman
```

pour entrer dans le container on a juste a entrer via `docker exec -it <names ou id> sh`
on peut voir que nous somme dans le container puisque nous somme root, mais il y a d'autre élément qui le montreront plus en profondeur:

#### processus
il suffit de faire la commande `top` pour s'apercevoir qu'il est vide:
```
$> top
Mem: 1720148K used, 129312K free, 15420K shrd, 120K buff, 492564K cached
CPU:   2% usr   1% sys   0% nic  95% idle   0% io   0% irq   0% sirq
Load average: 0.39 0.09 0.03 2/679 13
  PID  PPID USER     STAT   VSZ %VSZ CPU %CPU COMMAND
    6     0 root     S     1628   0%   0   0% sh
   13     6 root     R     1564   0%   1   0% top
    1     0 root     S     1552   0%   1   0% sleep 9999
```

#### réseaux
les cartes réseaux ne se nomment plus "ens33" ou "wlan0" mais "eth0@if15":
```
/ # ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
14: eth0@if15: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 02:42:ac:11:00:04 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.4/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

#### utilisateur
le fichier passwd ne comporte plus grand chose:
```
/ # cat /etc/passwd 
root:x:0:0:root:/root:/bin/ash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
news:x:9:13:news:/usr/lib/news:/sbin/nologin
uucp:x:10:14:uucp:/var/spool/uucppublic:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
man:x:13:15:man:/usr/man:/sbin/nologin
postmaster:x:14:12:postmaster:/var/spool/mail:/sbin/nologin
cron:x:16:16:cron:/var/spool/cron:/sbin/nologin
ftp:x:21:21::/var/lib/ftp:/sbin/nologin
sshd:x:22:22:sshd:/dev/null:/sbin/nologin
at:x:25:25:at:/var/spool/cron/atjobs:/sbin/nologin
squid:x:31:31:Squid:/var/cache/squid:/sbin/nologin
xfs:x:33:33:X Font Server:/etc/X11/fs:/sbin/nologin
games:x:35:35:games:/usr/games:/sbin/nologin
postgres:x:70:70::/var/lib/postgresql:/bin/sh
cyrus:x:85:12::/usr/cyrus:/sbin/nologin
vpopmail:x:89:89::/var/vpopmail:/sbin/nologin
ntp:x:123:123:NTP:/var/empty:/sbin/nologin
smmsp:x:209:209:smmsp:/var/spool/mqueue:/sbin/nologin
guest:x:405:100:guest:/dev/null:/sbin/nologin
nobody:x:65534:65534:nobody:/:/sbin/nologin
```

#### Partition
le root est remplacé par un "overlay" et certain fichier du dossier etc sont relié différemment:
```
/ # df -h
Filesystem                Size      Used Available Use% Mounted on
overlay                  26.9G      5.9G     21.0G  22% /
tmpfs                    64.0M         0     64.0M   0% /dev
tmpfs                   903.1M         0    903.1M   0% /sys/fs/cgroup
/dev/mapper/cl-root      26.9G      5.9G     21.0G  22% /etc/resolv.conf
/dev/mapper/cl-root      26.9G      5.9G     21.0G  22% /etc/hostname
/dev/mapper/cl-root      26.9G      5.9G     21.0G  22% /etc/hosts
shm                      64.0M         0     64.0M   0% /dev/shm
tmpfs                   903.1M         0    903.1M   0% /proc/acpi
tmpfs                    64.0M         0     64.0M   0% /proc/kcore
tmpfs                    64.0M         0     64.0M   0% /proc/keys
tmpfs                    64.0M         0     64.0M   0% /proc/timer_list
tmpfs                    64.0M         0     64.0M   0% /proc/sched_debug
tmpfs                   903.1M         0    903.1M   0% /proc/scsi
tmpfs                   903.1M         0    903.1M   0% /sys/firmware
```

une fois les tests réalisé, on arrete les containers et on supprime les dockers:
```
$> docker stop $(docker ps -q) && docker rm -f $(docker ps -a -q)
598685374e9a
8d2951cdacb4
598685374e9a
8d2951cdacb4
c00880b93833
f07ec4cf1906
a3a127d85389
64240f28b9e3
19b621e08b51
$> docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
$> 

```

### NGINX
on installe nginx en tant que daemon (-d) et on le lance sur le port 80 (-p 80:80):
```
$> docker run -d -p 80:80 nginx
6da48a815b7c018555d4da5396631e1ade21eea1872c92ea1051f5d1bbcb9359
```

on vérifie qu'il est fonctionnel sur notre PC en faisant:
```html
$> curl localhost:80
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

## Gestion d'image
Le docker d'apache ne se nomme pas apache mais httpd comme indiquer la base des image de docker:
`https://hub.docker.com/_/httpd`

pour l'installer en version 2.2 il suffit d'écrire le repo suivit de deux-point puis la version:
```
$> docker pull httpd:2.2 
2.2: Pulling from library/httpd
f49cf87b52c1: Downloading [=================================================> ]  52.09MB/52.6MB
24b1e09cbcb7: Download complete 
8a4e0d64e915: Download complete 
bcbe0eb4ca51: Download complete 
16e370c15d38: Download complete 
```

une fois cela fait, je le lance sur le port "8080" vu que le port 80 est déjà pris par NGINX:
```
$> docker run -d -p 8080:80 httpd:2.2 
48fc98e1ff2fde3b8596763576039253708e3f023e1ac184b120c0b31cd78f94
$> curl localhost:8080
<html><body><h1>It works!</h1></body></html>%     
```

### Création d'un docker
Pour crée une image, il faut crée un fichier nommé "Dockerfile"
on nous demande de crée un Dockerfile qui doit comporter ces éléments: basé sur alpine, Python3, EXPOSE, WORKDIR, COPY

voici mon Dockerfile:
```
# Image de base
FROM alpine:latest

# Installation de Python3
RUN apk update && apk add python3

# On expose le port 8888
EXPOSE 8888

# Changement du repertoire courant
WORKDIR /app

# Ajout d'une image dans le dossier app
ADD http://www.foo.com/media/W1siZiIsIjIwMTIvMDQvMjYvMjAvMTEvNDkvNDI2L2NvcmRvdmFiZWFjaC5qcGciXSxbInAiLCJ0aHVtYiIsIjc1MHgyMDAjIl1d/cordovabeach.jpg /app/

# On lance le serveur quand on démarre le conteneur
CMD [ "python3", "-m", "http.server", "8888;" ]
```


Pour acceder au serveur au service web, mais également au dossier "app" depuis le dossier courant et non le container, nous avons besoin de 2 choses:
* un port
* monter un volume 

pour faire cela,
on crée un dossier `partage` dans le répertoire courant puis
on build l'image en faisant: `docker build . -t <NomDeContainer>`
et on le run pour lancer un container avec les options que l'on veut:
`docker run -d -p "8888:8888" -v $(pwd)/partage:/app <NomDeContainer>` (avec -d pour le lancer en daemon)

quand on cherche à y acceder avec `docker exec -it <NomContainer> sh` on remarque que l'image téléchargé n'existe plus dans le "/app" elle s'est fait overider par le dossier "partage" que nous avons crée

```
$> curl localhost:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="fichierPC.txt">fichierPC.txt</a></li>
</ul>
<hr>
</body>
</html>
```

### Manipulation daemon docker

Le fichier docker.json n'existant pas j'ai du le crée
et le fichier docker.sock se trouve précisément dans: `/var/run/docker.sock` ou pour certain système il se trouve également dans `/run/docker.sock`

Si on veux lancer docker avec un socket, il suffit de lui indiquer dans le daemon qu'on utilise le TCP avec un port
```
ExecStart=/usr/bin/docker daemon -H fd:// -H tcp://192.168.223.15:2375
```

le "data-root" de docker par défaut se trouve dans `/var/lib/docker` nous le copions (cp -r)  dans `/data/docker`

Le OOM (Out Of Memory) est une feature du kernel qui détecte si un process consomme de plus en plus de mémoire, une fois activé, le kernel va automatiquement kill le processus pour éviter un problème de performance sur le système, par défaut pour Linux, tous les process sont égaux (0), changer la valeur va indiquer au noyaux qu'il va devoir traiter différemment avec ce processus.
Sous Docker la variable se nomme `--oom-score-adj=` et peut aller de -1000 à +1000 (0 étant par défaut)

Personnellement je reste sur 0 parce que je ne sais pas vraiment comment fonctionne réellement ce truc et que je ne veux pas me retrouver à chercher "pourquoi mon processus à crash" alors qu'il est censé fonctionner correctement.


## Docker-compose
### V1
Après avoir installer Docker-compose ajouter les droits au binaire docker-compose (chmod a+rx) et lancer le docker-compose.yml avec `docker-compose up` nous devions crée notre propre docker-compose avec l'image de python.

J'ai donc fait ceci:
```yaml
version: "3.7"

services:
  pyweb:
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - "8888:8888"
    networks:
      - overlay

networks:
  overlay:

```

build:
  context et dockerfile sont les 2 informations pour expliquer que l'image est un truc local à build.

vu que le fichier se nommais `docker-compose-v1.yml` j'ai du ajouter le nom dans la ligne de commande et le build comme ceci:
```
$> docker-compose -f docker-compose-v1.yml up --build -d
Building pyweb
Step 1/6 : FROM alpine:latest
 ---> 965ea09ff2eb
Step 2/6 : RUN apk update && apk add python3
 ---> Using cache
 ---> 032765cc002d
Step 3/6 : EXPOSE 8888
 ---> Using cache
 ---> 6c9c07638698
Step 4/6 : WORKDIR /app
 ---> Using cache
 ---> dfc70442ddef
Step 5/6 : ADD http://www.foo.com/media/W1siZiIsIjIwMTIvMDQvMjYvMjAvMTEvNDkvNDI2L2NvcmRvdmFiZWFjaC5qcGciXSxbInAiLCJ0aHVtYiIsIjc1MHgyMDAjIl1d/cordovabeach.jpg /app/

 ---> Using cache
 ---> 8f6afc9342b4
Step 6/6 : CMD [ "python3", "-m", "http.server", "8888" ]
 ---> Using cache
 ---> 7f5e1fed85d4

Successfully built 7f5e1fed85d4
Successfully tagged tp1_pyweb:latest
Creating tp1_pyweb_1 ... done
Attaching to tp1_pyweb_1

```

le -d c'est pour le lancer en daemon 

on peut vérifier qu'il tourne bien avec un docker ps, et qu'il se trouve bien sur le port "8080" sur le port entrant et sortant
```
$> docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
4ddaef3a68d9        compose_pyweb       "python3 -m http.ser…"   18 seconds ago      Up 2 seconds        0.0.0.0:8888->8888/tcp   tp1_pyweb_1
```

par contre bien que le service tourne et la page web est accessible, il n'y a aucun log lorsque l'on fait:
`docker logs tp1_pyweb_1`

### V2
On nous as demandé cette fois-ci de faire reverse proxy avec sécurité HTTPS sur port 443, j'ai donc commencé par crée une paire de clé:
```bash
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./nginx/certs/nginx.key -out ./nginx/certs/nginx.crt
Generating a RSA private key
........................................................+++++
...........+++++
writing new private key to './nginx/certs/nginx.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:None
Locality Name (eg, city) [Default City]:
Organization Name (eg, company) [Default Company Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server's hostname) []:
Email Address []:none@none.none

```


une fois cela fait, j'ai pris la config du fichier nginx existant sur le repo et je l'ai modifié pour l'adapté à mon environement
```json
server {
    server_name   test.docker;
    listen        443 ssl;

    ssl_certificate           /certs/nginx.crt;
    ssl_certificate_key       /certs/nginx.key;

    proxy_set_header        Host $host;
    proxy_set_header        X-Real-IP $remote_addr;
    proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header        X-Forwarded-Proto $scheme;

    location      / {
	    proxy_pass http://pyweb:8888/;
    }
}
```

et voici le docker-compose.yml
```yaml
version: "3.7"

services:
  pyweb:
    build:
      context: ./webserver
      dockerfile: Dockerfile
    ports:
      - "8888:8888"
    networks:
      - overlay

  server:
    image: nginx
    volumes:
      - ./nginx/conf/nginx.conf:/etc/nginx/conf.d/test.docker.conf
      - ./nginx/certs:/certs
    ports:
      - "443:443"
    networks:
      - overlay
    depends_on:
      - pyweb
    restart: always

networks:
  overlay:
```

maintenant on execute:
```bash
$> docker-compose -f docker-compose-v2.yml up --build
Building pyweb
Step 1/6 : FROM alpine:latest
 ---> 965ea09ff2eb
Step 2/6 : RUN apk update && apk add python3
 ---> Using cache
 ---> 032765cc002d
Step 3/6 : EXPOSE 8888
 ---> Using cache
 ---> 6c9c07638698
Step 4/6 : WORKDIR /app
 ---> Using cache
 ---> dfc70442ddef
Step 5/6 : ADD http://www.foo.com/media/W1siZiIsIjIwMTIvMDQvMjYvMjAvMTEvNDkvNDI2L2NvcmRvdmFiZWFjaC5qcGciXSxbInAiLCJ0aHVtYiIsIjc1MHgyMDAjIl1d/cordovabeach.jpg /app/

 ---> Using cache
 ---> 8f6afc9342b4
Step 6/6 : CMD [ "python3", "-m", "http.server", "8888" ]
 ---> Using cache
 ---> 7f5e1fed85d4

Successfully built 7f5e1fed85d4
Successfully tagged tp1_pyweb:latest
Starting tp1_pyweb_1 ... done
Creating tp1_server_1 ... done
Attaching to tp1_pyweb_1, tp1_server_1
server_1  | 172.20.0.1 - - [06/Jan/2020:05:49:51 +0000] "GET / HTTP/1.1" 400 255 "-" "Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0" "-"
server_1  | 172.20.0.1 - - [06/Jan/2020:05:49:51 +0000] "GET /favicon.ico HTTP/1.1" 400 255 "-" "Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0" "-"
server_1  | 172.20.0.1 - - [06/Jan/2020:05:50:20 +0000] "GET / HTTP/1.1" 200 354 "-" "Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0" "-"
server_1  | 172.20.0.1 - - [06/Jan/2020:05:50:20 +0000] "GET /favicon.ico HTTP/1.1" 404 469 "-" "Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0" "-"
```

suivi d'un docker ps:
```
$> docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS                          NAMES
10638d64f2ee        nginx               "nginx -g 'daemon of…"   About a minute ago   Up About a minute   80/tcp, 0.0.0.0:443->443/tcp   tp1_server_1
4ddaef3a68d9        tp1_pyweb           "python3 -m http.ser…"   About an hour ago    Up About a minute   0.0.0.0:8888->8888/tcp         tp1_pyweb_1
```


#### Test final
comme on a pu le voir plus haut, les logs indique 2 connexions via un navigateur web firefox, je suis accedé au site via https://localhost:443
j'ai du accepter le certificat et j'ai enfin pu voir la page de python

un test sous curl en donnera plus d'information:
```bash
$> curl https://localhost
curl: (60) SSL certificate problem: self signed certificate
More details here: https://curl.haxx.se/docs/sslcerts.html

curl failed to verify the legitimacy of the server and therefore could not
establish a secure connection to it. To learn more about this situation and
how to fix it, please visit the web page mentioned above.
```

déjà juste la connexion au serveur NGINX fonctionne, on se connecte maintenant de façon non sécurisé:
```bash
$> curl --insecure https://localhost
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="cordovabeach.jpg">cordovabeach.jpg</a></li>
</ul>
<hr>
</body>
</html>
```

on retrouve notre fichier et la page qu'on est censé trouver sur python.
