# TP3

## Setup Docker Swarm

ici j'ai crée 3 VM CentOS 7 et les 3 reliés sur 1 réseaux virtuel en Host only
J'y ai installé docker/docker-compose, changer le hostname des 3 VM, et j'ai ensuite crée un docker swarm "maitre" sur la première machine:
```bash
node 1 $> docker swarm init --advertise-addr 192.168.31.128
Swarm initialized: current node (kgjko4eou4uzc0nmllt7clxf2) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-1pjfeis8qc4dram7ggd4vl8teodmol2vs63r5cnery5f496nyx-d9kek8gspi41mvbo5n19k1ciz 192.168.31.128:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

node 1 $> docker swarm join-token worker                   
To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-1pjfeis8qc4dram7ggd4vl8teodmol2vs63r5cnery5f496nyx-d9kek8gspi41mvbo5n19k1ciz 192.168.31.128:2377

node 1 $> docker swarm join-token manager
To add a manager to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-1pjfeis8qc4dram7ggd4vl8teodmol2vs63r5cnery5f496nyx-71p662igcavhpuyo10m1rsbjd 192.168.31.128:2377
```

dans mon cas, toutes les machines doivent être en "manager"
du coup sur les 2 autres, je n'ai eu qu'à y ajouter la commande que l'on voit ci-dessus:
```bash
node 3 $> docker swarm join --token SWMTKN-1-1pjfeis8qc4dram7ggd4vl8teodmol2vs63r5cnery5f496nyx-71p662igcavhpuyo10m1rsbjd 192.168.31.128:2377
This node joined a swarm as a manager.
```

et pour vérifier qu'ils sont existant et fonctionnel:
```bash
node 1 $>docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
kgjko4eou4uzc0nmllt7clxf2 *   node1.b3            Ready               Active              Leader              19.03.6
ztc6dq93nr10eosczizr2q78e     node2.b3            Ready               Active              Reachable           19.03.6
avdgm2mo98zjwy0t1y5z4strj     node3.b3            Ready               Active              Reachable           19.03.6
```

## Première application

On va crée un stack docker avec le dockerfile du TP1,
cependant, docker stack ne peut pas build les images, il faut les builds à la main avant de pouvoir lancer docker stack:


`node 1 $>vim webserver/Dockerfile`
```docker
# Image de base
FROM alpine:latest

# Installation de Python3
RUN apk update && apk add python3

# On expose le port 8888
EXPOSE 8888

# Changement du repertoire courant
WORKDIR /app

# Ajout d'une image dans le dossier app
ADD http://www.foo.com/media/W1siZiIsIjIwMTIvMDQvMjYvMjAvMTEvNDkvNDI2L2NvcmRvdmFiZWFjaC5qcGciXSxbInAiLCJ0aHVtYiIsIjc1MHgyMDAjIl1d/cordovabeach.jpg /app/

# On lance le serveur quand on démarre le conteneur
CMD [ "python3", "-m", "http.server", "8888" ]
```

on duplique le fichier et on le build sur chacune des VMs:
```bash
$>docker build -t pyweb webserver/.           
Sending build context to Docker daemon  2.048kB
Step 1/6 : FROM alpine:latest
latest: Pulling from library/alpine
c9b1b535fdd9: Pull complete 
Digest: sha256:ab00606a42621fb68f2ed6ad3c88be54397f981a7b70a79db3d1172b11c4367d
Status: Downloaded newer image for alpine:latest
 ---> e7d92cdc71fe
Step 2/6 : RUN apk update && apk add python3
 ---> Running in 25126d1e641f
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/community/x86_64/APKINDEX.tar.gz
v3.11.3-71-g8230b2bd71 [http://dl-cdn.alpinelinux.org/alpine/v3.11/main]
v3.11.3-60-gb3a10d424a [http://dl-cdn.alpinelinux.org/alpine/v3.11/community]
OK: 11262 distinct packages available
(1/11) Installing libbz2 (1.0.8-r1)
(2/11) Installing expat (2.2.9-r1)
(3/11) Installing libffi (3.2.1-r6)
(4/11) Installing gdbm (1.13-r1)
(5/11) Installing xz-libs (5.2.4-r0)
(6/11) Installing ncurses-terminfo-base (6.1_p20191130-r0)
(7/11) Installing ncurses-terminfo (6.1_p20191130-r0)
(8/11) Installing ncurses-libs (6.1_p20191130-r0)
(9/11) Installing readline (8.0.1-r0)
(10/11) Installing sqlite-libs (3.30.1-r1)
(11/11) Installing python3 (3.8.1-r0)
Executing busybox-1.31.1-r9.trigger
OK: 71 MiB in 25 packages
Removing intermediate container 25126d1e641f
 ---> 0d1e8400278b
Step 3/6 : EXPOSE 8888
 ---> Running in 03b592d4f850
Removing intermediate container 03b592d4f850
 ---> 11fc532893ae
Step 4/6 : WORKDIR /app
 ---> Running in 3fa2e716d3c1
Removing intermediate container 3fa2e716d3c1
 ---> 2fe21fd9e144
Step 5/6 : ADD http://www.foo.com/media/W1siZiIsIjIwMTIvMDQvMjYvMjAvMTEvNDkvNDI2L2NvcmRvdmFiZWFjaC5qcGciXSxbInAiLCJ0aHVtYiIsIjc1MHgyMDAjIl1d/cordovabeach.jpg /app/
Downloading   46.5kB/46.5kB
 ---> cd64c424fdd7
Step 6/6 : CMD [ "python3", "-m", "http.server", "8888" ]
 ---> Running in 472f992273d2
Removing intermediate container 472f992273d2
 ---> 464ee2ccedbb
Successfully built 464ee2ccedbb
Successfully tagged pyweb:latest
```

ensuite, on modifie le docker-compose pour qu'il n'utilise pas "build" mais "image" directement:

`node 1 $>vim docker-compose-v1.yml`
```docker
version: "3.7"

services:
  pyweb:
    image: pyweb
    ports:
      - "8888:8888"
    networks:
      - overlay

networks:
  overlay:
```

et ensuite, plus qu'à déployer:
```bash
node 1 $> docker stack deploy -c docker-compose-v1.yml python_web
Creating service python_web_pyweb
```

on vérifie que celui-ci est donc bien déployé et en fonctionnement:
```bash
node 1 $>docker stack ls
NAME                SERVICES            ORCHESTRATOR
python_web          1                   Swarm
$> docker stack services python_web
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
lj59t5q72icj        python_web_pyweb    replicated          1/1                 pyweb:latest        *:8888->8888/tcp
$> docker service ps python_web_pyweb
ID                  NAME                 IMAGE               NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
o7ke012cp6yv        python_web_pyweb.1   pyweb:latest        node2.b3            Running             Running 23 seconds ago
```

un `docker ps` sur node 2 PUIS sur le node 3:
```
node 2 $>docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
7280a6898e94        pyweb:latest        "python3 -m http.ser…"   12 minutes ago      Up 12 minutes       8888/tcp            python_web_pyweb.1.z45bb6kvyj2m03o58db6cpt12
```
```
node 3 $>docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
```

comme on le vois un peu plus haut, le service possède un port "8888", avec `ss` on peut voir avec quel service il fonctionne:
```bash
#> ss -lntp                     
State       Recv-Q Send-Q   Local Address:Port                  Peer Address:Port              
LISTEN      0      100          127.0.0.1:25                               *:*                   users:(("master",pid=1101,fd=13))
LISTEN      0      128                  *:22                               *:*                   users:(("sshd",pid=904,fd=3))
LISTEN      0      128               [::]:8888                          [::]:*                   users:(("dockerd",pid=956,fd=38))
LISTEN      0      100              [::1]:25                            [::]:*                   users:(("master",pid=1101,fd=14))
LISTEN      0      128               [::]:2377                          [::]:*                   users:(("dockerd",pid=956,fd=17))
LISTEN      0      128               [::]:7946                          [::]:*                   users:(("dockerd",pid=956,fd=29))
LISTEN      0      128               [::]:22                            [::]:*                   users:(("sshd",pid=904,fd=4))
```

on voit également que docker communique avec 2 autres ports: "2377" et "7946".
Le port 2377 sert pour la configuration du cluster et la synchronisation raft
Le port 7946 sert pour la communication et la découverte entre les différents nodes

on augmente le nombre d'instance et on vérifie son fonctionnement:
```bash
node 1 $> docker stack ls   
NAME                SERVICES            ORCHESTRATOR
python_web          1                   Swarm
node 1 $> docker stack services python_web
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
lj59t5q72icj        python_web_pyweb    replicated          1/1                 pyweb:latest        *:8888->8888/tcp

node 1 $> docker service scale python_web_pyweb=2
python_web_pyweb scaled to 2
overall progress: 2 out of 2 tasks 
1/2: running   
2/2: running   
verify: Service converged

node 1 $> docker stack services python_web
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
lj59t5q72icj        python_web_pyweb    replicated          2/2                 pyweb:latest        *:8888->8888/tcp

node 1 $>docker service ps python_web_pyweb
ID                  NAME                     IMAGE               NODE                DESIRED STATE       CURRENT STATE               ERROR                         PORTS
z45bb6kvyj2m        python_web_pyweb.1       pyweb:latest        node2.b3            Running             Running about an hour ago                                 
sf5nwllrgvbj        python_web_pyweb.2       pyweb:latest        node3.b3            Running             Running 16 minutes ago
```

pour les tests on va essayer d'en déconnecter un, genre, celui du node 3:
```bash
node 3 $>docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
482cbc2ced6d        pyweb:latest        "python3 -m http.ser…"   23 minutes ago      Up 23 minutes       8888/tcp            python_web_pyweb.2.sf5nwllrgvbjt6udqxhzi6nt7
node 3 $>poweroff
```

comme on le vois avec node 1, comme le node 3 est éteint, il à été relancé sur le node 1:
```bash
node 1 $>docker service ps python_web_pyweb
ID                  NAME                     IMAGE               NODE                DESIRED STATE       CURRENT STATE               ERROR                         PORTS
z45bb6kvyj2m        python_web_pyweb.1       pyweb:latest        node2.b3            Running             Running about an hour ago                                 
iheaul8oxljx        python_web_pyweb.2       pyweb:latest        node1.b3            Running             Running 5 seconds ago                                     
sf5nwllrgvbj         \_ python_web_pyweb.2   pyweb:latest        node3.b3            Shutdown            Running 25 seconds ago 
```

et après un test on voit qu'il est toujours possible de contacter le container via le port 8888:
```bash
$>curl node1.b3:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="cordovabeach.jpg">cordovabeach.jpg</a></li>
</ul>
<hr>
</body>
</html>
$>curl node2.b3:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="cordovabeach.jpg">cordovabeach.jpg</a></li>
</ul>
<hr>
</body>
</html>
```

## Construction d'un échosystème


