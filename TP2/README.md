# TP2

## Gestion de container

ici j'ai lancé 3 docker container:
```bash
$>docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
5691ef7867cd        alpine              "sleep 9999"        14 minutes ago      Up 14 minutes                           youthful_lamarr
24c639db395e        alpine              "sleep 9999"        14 minutes ago      Up 14 minutes                           thirsty_fermi
926f9336be0a        alpine              "sleep 9999"        14 minutes ago      Up 14 minutes                           relaxed_mayer
```


quand on lance un PS on remarque que le container as plusieurs information comme le PID et le PPID le PPID est le processus parent auquel il se rapporte et le PID est le numéro du processus actuel
```bash
$> ps -ef | grep docker
UID        PID    PPID  C STIME TTY      TIME     CMD
root       6486      1  0 12:15 ?        00:00:01 /usr/bin/dockerd -H fd://
root       7741   6338  0 13:04 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/926f9336be0abd792f330c50233ee66130f64115852ca6b3baf4d7cff6b7d8c7 -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc
root       7817   6338  0 13:04 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/24c639db395e60b47d260ff66aa10eca26a8d86abce9156322b14809fc6c184f -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc
root       7898   6338  0 13:04 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/5691ef7867cd806b7b3e415c37e2b8ee8bad621dc32b6d4bb08fd7f24ef86997 -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc
anon       8386   8058  0 13:17 pts/2    00:00:00 docker exec -it 5691ef7867cd /bin/sh

```

ici on voit que "containerd-shim" est relier à un processus parent, on fait donc un grep de son PPID:
```bash
$> ps -ef | grep 6338  
root       6338      1  0 12:14 ?        00:00:11 /usr/bin/containerd
root       7741   6338  0 13:04 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/926f9336be0abd792f330c50233ee66130f64115852ca6b3baf4d7cff6b7d8c7 -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc
root       7817   6338  0 13:04 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/24c639db395e60b47d260ff66aa10eca26a8d86abce9156322b14809fc6c184f -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc
root       7898   6338  0 13:04 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/5691ef7867cd806b7b3e415c37e2b8ee8bad621dc32b6d4bb08fd7f24ef86997 -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc
```

et on voit que le process auquel le PPID est relier se nomme "containerd" et que tout ce beau monde (containerd et dockerd) sont relié au processus "init"

Docker en lui-même n'est qu'un client qui communique avec containerd
containerd va lancer un appel à runc qui va juste lancer un containerd-shrim avant de s'arreté.

## API HTTP DOCKER
### Liste des containers

Dockerd peut fonctionner avec une API HTTP, on peut y acceder en utilisant les sockets Unix comme cela:

```bash
$>curl --unix-socket /var/run/docker.sock http://localhost/containers/json
[{"Id":"048130badec16a823c56cfa4e6377342f7384a382838be88eb07c72dc095b0b9","Names":["/focused_meninsky"],"Image":"alpine","ImageID":"sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34","Command":"sleep 99999","Created":1578404833,"Ports":[],"Labels":{},"State":"running","Status":"Up 26 seconds","HostConfig":{"NetworkMode":"default"},"NetworkSettings":{"Networks":{"bridge":{"IPAMConfig":null,"Links":null,"Aliases":null,"NetworkID":"bea9915bc6115cb21a988d71eb0a8018e1e2c271c617fd56638d82b3bd0cfe0d","EndpointID":"33a70094752396af4a6205b355c6630b9d2e19b1a6049c783fdc7ca471a6a4ea","Gateway":"172.17.0.1","IPAddress":"172.17.0.4","IPPrefixLen":16,"IPv6Gateway":"","GlobalIPv6Address":"","GlobalIPv6PrefixLen":0,"MacAddress":"02:42:ac:11:00:04","DriverOpts":null}}},"Mounts":[]},{"Id":"a91dff7f81031f605b4eb75b723d2d4e0c12ce488407ef96dae56574bfe0034e","Names":["/optimistic_shockley"],"Image":"alpine","ImageID":"sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34","Command":"sleep 99999","Created":1578404832,"Ports":[],"Labels":{},"State":"running","Status":"Up 27 seconds","HostConfig":{"NetworkMode":"default"},"NetworkSettings":{"Networks":{"bridge":{"IPAMConfig":null,"Links":null,"Aliases":null,"NetworkID":"bea9915bc6115cb21a988d71eb0a8018e1e2c271c617fd56638d82b3bd0cfe0d","EndpointID":"f7c76a9a6b98691d664aca557f842e106288f3f6a7e19e56dbc011aea45619d1","Gateway":"172.17.0.1","IPAddress":"172.17.0.3","IPPrefixLen":16,"IPv6Gateway":"","GlobalIPv6Address":"","GlobalIPv6PrefixLen":0,"MacAddress":"02:42:ac:11:00:03","DriverOpts":null}}},"Mounts":[]},{"Id":"44cc3412e85c3cfa156cb7971dfac66a8692b09fe057094190ccb0b82aee2b2d","Names":["/mystifying_heyrovsky"],"Image":"alpine","ImageID":"sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34","Command":"sleep 99999","Created":1578404830,"Ports":[],"Labels":{},"State":"running","Status":"Up 29 seconds","HostConfig":{"NetworkMode":"default"},"NetworkSettings":{"Networks":{"bridge":{"IPAMConfig":null,"Links":null,"Aliases":null,"NetworkID":"bea9915bc6115cb21a988d71eb0a8018e1e2c271c617fd56638d82b3bd0cfe0d","EndpointID":"ed80444558b327663023afb31416fb2d8cb2698f127b4bb53fe1741a4ac609f8","Gateway":"172.17.0.1","IPAddress":"172.17.0.2","IPPrefixLen":16,"IPv6Gateway":"","GlobalIPv6Address":"","GlobalIPv6PrefixLen":0,"MacAddress":"02:42:ac:11:00:02","DriverOpts":null}}},"Mounts":[]}]
```

une vue plus propre donne cela:
```json
[
  {
    "Id": "048130badec16a823c56cfa4e6377342f7384a382838be88eb07c72dc095b0b9",
    "Names": [
      "/focused_meninsky"
    ],
    "Image": "alpine",
    "ImageID": "sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34",
    "Command": "sleep 99999",
    "Created": 1578404833,
    "Ports": [],
    "Labels": {},
    "State": "running",
    "Status": "Up 26 seconds",
    "HostConfig": {
      "NetworkMode": "default"
    },
    "NetworkSettings": {
      "Networks": {
        "bridge": {
          "IPAMConfig": null,
          "Links": null,
          "Aliases": null,
          "NetworkID": "bea9915bc6115cb21a988d71eb0a8018e1e2c271c617fd56638d82b3bd0cfe0d",
          "EndpointID": "33a70094752396af4a6205b355c6630b9d2e19b1a6049c783fdc7ca471a6a4ea",
          "Gateway": "172.17.0.1",
          "IPAddress": "172.17.0.4",
          "IPPrefixLen": 16,
          "IPv6Gateway": "",
          "GlobalIPv6Address": "",
          "GlobalIPv6PrefixLen": 0,
          "MacAddress": "02:42:ac:11:00:04",
          "DriverOpts": null
        }
      }
    },
    "Mounts": []
  },
  {
    "Id": "a91dff7f81031f605b4eb75b723d2d4e0c12ce488407ef96dae56574bfe0034e",
    "Names": [
      "/optimistic_shockley"
    ],
    "Image": "alpine",
    "ImageID": "sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34",
    "Command": "sleep 99999",
    "Created": 1578404832,
    "Ports": [],
    "Labels": {},
    "State": "running",
    "Status": "Up 27 seconds",
    "HostConfig": {
      "NetworkMode": "default"
    },
    "NetworkSettings": {
      "Networks": {
        "bridge": {
          "IPAMConfig": null,
          "Links": null,
          "Aliases": null,
          "NetworkID": "bea9915bc6115cb21a988d71eb0a8018e1e2c271c617fd56638d82b3bd0cfe0d",
          "EndpointID": "f7c76a9a6b98691d664aca557f842e106288f3f6a7e19e56dbc011aea45619d1",
          "Gateway": "172.17.0.1",
          "IPAddress": "172.17.0.3",
          "IPPrefixLen": 16,
          "IPv6Gateway": "",
          "GlobalIPv6Address": "",
          "GlobalIPv6PrefixLen": 0,
          "MacAddress": "02:42:ac:11:00:03",
          "DriverOpts": null
        }
      }
    },
    "Mounts": []
  },
  {
    "Id": "44cc3412e85c3cfa156cb7971dfac66a8692b09fe057094190ccb0b82aee2b2d",
    "Names": [
      "/mystifying_heyrovsky"
    ],
    "Image": "alpine",
    "ImageID": "sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34",
    "Command": "sleep 99999",
    "Created": 1578404830,
    "Ports": [],
    "Labels": {},
    "State": "running",
    "Status": "Up 29 seconds",
    "HostConfig": {
      "NetworkMode": "default"
    },
    "NetworkSettings": {
      "Networks": {
        "bridge": {
          "IPAMConfig": null,
          "Links": null,
          "Aliases": null,
          "NetworkID": "bea9915bc6115cb21a988d71eb0a8018e1e2c271c617fd56638d82b3bd0cfe0d",
          "EndpointID": "ed80444558b327663023afb31416fb2d8cb2698f127b4bb53fe1741a4ac609f8",
          "Gateway": "172.17.0.1",
          "IPAddress": "172.17.0.2",
          "IPPrefixLen": 16,
          "IPv6Gateway": "",
          "GlobalIPv6Address": "",
          "GlobalIPv6PrefixLen": 0,
          "MacAddress": "02:42:ac:11:00:02",
          "DriverOpts": null
        }
      }
    },
    "Mounts": []
  }
]
```

on remarque 3 "Id" et 3 "Names" qui correspondent effectivement au info que l'on peut voir avec un docker ps:
```
$>docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
048130badec1        alpine              "sleep 99999"       3 minutes ago       Up 3 minutes                            focused_meninsky
a91dff7f8103        alpine              "sleep 99999"       3 minutes ago       Up 3 minutes                            optimistic_shockley
44cc3412e85c        alpine              "sleep 99999"       3 minutes ago       Up 3 minutes                            mystifying_heyrovsky
```

### Liste des images
Actuellement je n'ai que alpine d'existant en image, je vais donc ajouter nginx et lister les images docker via les sockets HTTP
```bash
$> docker pull nginx
t tag: latest
latest: Pulling from library/nginx
8ec398bc0356: Pull complete 
465560073b6f: Pull complete 
f473f9fd0a8c: Pull complete 
Digest: sha256:b2d89d0a210398b4d1120b3e3a7672c16a4ba09c2c4a0395f18b9f7999b768f2
Status: Downloaded newer image for nginx:latest
docker.io/library/nginx:latest

$>curl --unix-socket /var/run/docker.sock http://localhost/images/json
[{"Containers":-1,"Created":1577546421,"Id":"sha256:f7bb5701a33c0e572ed06ca554edca1bee96cbbc1f76f3b01c985de7e19d0657","Labels":{"maintainer":"NGINX Docker Maintainers <docker-maint@nginx.com>"},"ParentId":"","RepoDigests":["nginx@sha256:b2d89d0a210398b4d1120b3e3a7672c16a4ba09c2c4a0395f18b9f7999b768f2"],"RepoTags":["nginx:latest"],"SharedSize":-1,"Size":126323778,"VirtualSize":126323778},{"Containers":-1,"Created":1577215212,"Id":"sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34","Labels":null,"ParentId":"","RepoDigests":["alpine@sha256:2171658620155679240babee0a7714f6509fae66898db422ad803b951257db78"],"RepoTags":["alpine:latest"],"SharedSize":-1,"Size":5591300,"VirtualSize":5591300}]
```

en plus propre:
```json
[
  {
    "Containers": -1,
    "Created": 1577546421,
    "Id": "sha256:f7bb5701a33c0e572ed06ca554edca1bee96cbbc1f76f3b01c985de7e19d0657",
    "Labels": {
      "maintainer": "NGINX Docker Maintainers <docker-maint@nginx.com>"
    },
    "ParentId": "",
    "RepoDigests": [
      "nginx@sha256:b2d89d0a210398b4d1120b3e3a7672c16a4ba09c2c4a0395f18b9f7999b768f2"
    ],
    "RepoTags": [
      "nginx:latest"
    ],
    "SharedSize": -1,
    "Size": 126323778,
    "VirtualSize": 126323778
  },
  {
    "Containers": -1,
    "Created": 1577215212,
    "Id": "sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34",
    "Labels": null,
    "ParentId": "",
    "RepoDigests": [
      "alpine@sha256:2171658620155679240babee0a7714f6509fae66898db422ad803b951257db78"
    ],
    "RepoTags": [
      "alpine:latest"
    ],
    "SharedSize": -1,
    "Size": 5591300,
    "VirtualSize": 5591300
  }
]
```

comme on le vois, il y a 2 images, celle de NGINX:latest et celle de alpine:latest
c'est l'équivalent du docker image ls
```bash
$>docker image ls
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
nginx               latest              f7bb5701a33c        9 days ago          126MB
alpine              latest              cc0abc535e36        13 days ago         5.59MB
```

## Sandboxing Namespace
le `/proc/self` permet de lister le processus courant, on va donc l'utiliser pour identifier les différents types de namespace que mon shell utilise:
```bash
sh-4.4$ ls -al /proc/self/ns
total 0
dr-x--x--x. 2 anon anon 0 Jan  7 15:20 .
dr-xr-xr-x. 9 anon anon 0 Jan  7 15:20 ..
lrwxrwxrwx. 1 anon anon 0 Jan  7 15:20 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 15:20 ipc -> 'ipc:[4026531839]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 15:20 mnt -> 'mnt:[4026531840]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 15:20 net -> 'net:[4026531992]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 15:20 pid -> 'pid:[4026531836]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 15:20 pid_for_children -> 'pid:[4026531836]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 15:20 user -> 'user:[4026531837]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 15:20 uts -> 'uts:[4026531838]'
```

on s'aperçois que le namespace correspond à celui du systemd:
```bash
$>lsns
        NS TYPE   NPROCS   PID USER COMMAND
4026531835 cgroup     71  7885 anon /usr/lib/systemd/systemd --user
4026531836 pid        71  7885 anon /usr/lib/systemd/systemd --user
4026531837 user       63  7885 anon /usr/lib/systemd/systemd --user
4026531838 uts        71  7885 anon /usr/lib/systemd/systemd --user
4026531839 ipc        63  7885 anon /usr/lib/systemd/systemd --user
4026531840 mnt        71  7885 anon /usr/lib/systemd/systemd --user
4026531992 net        63  7885 anon /usr/lib/systemd/systemd --user
```

### Accès à un namespace
si on veut crée un nouveau namespace on utilise `unshare`
```bash
sh-4.4$ echo $$
64028
sh-4.4$ ls -la /proc/self/ns
total 0
dr-x--x--x. 2 anon anon 0 Jan  7 16:03 .
dr-xr-xr-x. 9 anon anon 0 Jan  7 16:03 ..
lrwxrwxrwx. 1 anon anon 0 Jan  7 16:03 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 16:03 ipc -> 'ipc:[4026531839]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 16:03 mnt -> 'mnt:[4026531840]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 16:03 net -> 'net:[4026531992]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 16:03 pid -> 'pid:[4026531836]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 16:03 pid_for_children -> 'pid:[4026531836]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 16:03 user -> 'user:[4026531837]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 16:03 uts -> 'uts:[4026531838]'
sh-4.4$ unshare -fnmpU /bin/bash
[nobody@localhost ~]$ echo $$
1
[nobody@localhost ~]$ lsns
        NS TYPE   NPROCS   PID USER   COMMAND
4026531835 cgroup      2 64406 nobody /bin/bash
4026531838 uts         2 64406 nobody /bin/bash
4026531839 ipc         2 64406 nobody /bin/bash
4026533438 user        2 64406 nobody /bin/bash
4026533440 mnt         2 64406 nobody /bin/bash
4026533441 pid         2 64406 nobody /bin/bash
4026533443 net         2 64406 nobody /bin/bash

[nobody@localhost ~]$ ls -la /proc/self/ns
total 0
dr-x--x--x. 2 nobody nobody 0 Jan  7 16:23 .
dr-xr-xr-x. 9 nobody nobody 0 Jan  7 16:23 ..
lrwxrwxrwx. 1 nobody nobody 0 Jan  7 16:23 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx. 1 nobody nobody 0 Jan  7 16:23 ipc -> 'ipc:[4026531839]'
lrwxrwxrwx. 1 nobody nobody 0 Jan  7 16:23 mnt -> 'mnt:[4026533440]'
lrwxrwxrwx. 1 nobody nobody 0 Jan  7 16:23 net -> 'net:[4026533443]'
lrwxrwxrwx. 1 nobody nobody 0 Jan  7 16:23 pid -> 'pid:[4026533441]'
lrwxrwxrwx. 1 nobody nobody 0 Jan  7 16:23 pid_for_children -> 'pid:[4026533441]'
lrwxrwxrwx. 1 nobody nobody 0 Jan  7 16:23 user -> 'user:[4026533438]'
lrwxrwxrwx. 1 nobody nobody 0 Jan  7 16:23 uts -> 'uts:[4026531838]'
```

comme on le voit en faisant un unshare en y ajoutant des paramètres, on vois que le PID du processus est totalement différent,
et que lsns n'affiche plus que le bash avec des namespaces différents de celui lancé sur systemd.
(le -f fork est nécessaire sur la commande unshare pour avoir accès à la RAM et donc, faire des commandes)


### Namespace processus
sur Docker, 
pour connaitre le namespace de mon container,
je fais un `docker ps` pour connaitre le container ID. Ce container ID est le début de la longue chaine UUID, et avec un `systemd-cgls`, on repère le PID qui est associé à l'UUID:
```bash
$>docker ps   
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
c5c17b46b997        debian              "sleep 99999"       20 minutes ago      Up 20 minutes                           gifted_einstein
048130badec1        alpine              "sleep 99999"       2 hours ago         Up 2 hours                              focused_meninsky
a91dff7f8103        alpine              "sleep 99999"       2 hours ago         Up 2 hours                              optimistic_shockley
44cc3412e85c        alpine              "sleep 99999"       2 hours ago         Up 2 hours                              mystifying_heyrovsky

$>systemd-cgls 
Control group /:
-.slice
Control group /:
-.slice
├─docker
│ ├─a91dff7f81031f605b4eb75b723d2d4e0c12ce488407ef96dae56574bfe0034e
│ │ └─9534 sleep 99999
│ ├─c5c17b46b9978350f911b7b52affe8ba6bc4463b4c959343ed9d4c6c0e669a40
│ │ └─64642 sleep 99999
│ ├─048130badec16a823c56cfa4e6377342f7384a382838be88eb07c72dc095b0b9
│ │ └─9613 sleep 99999
│ └─44cc3412e85c3cfa156cb7971dfac66a8692b09fe057094190ccb0b82aee2b2d
│   └─9460 sleep 99999
├─user.slice
│ ├─user-42.slice
│ │ ├─session-c1.scope
│ │ │ ├─1205 gdm-session-worker [pam/gdm-launch-environment]
│ │ │ ├─1476 /usr/libexec/gdm-wayland-session gnome-session --autostart /usr/share/gdm/greeter/autostart
[...]
```

on peut également trouver son PID avec un `docker inspect <NomDuContainer>`:
```bash
docker inspect gifted_einstein | grep Pid
            "Pid": 64642,
            "PidMode": "",
            "PidsLimit": 0,
```


ensuite, il suffit d'afficher en root avec `ls -la` le processus du PID pour connaitre le bon namespace:
```bash
sh-4.4# ls -la /proc/64642/ns
total 0
dr-x--x--x. 2 root root 0 Jan  7 17:13 .
dr-xr-xr-x. 9 root root 0 Jan  7 17:11 ..
lrwxrwxrwx. 1 root root 0 Jan  7 17:13 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx. 1 root root 0 Jan  7 17:13 ipc -> 'ipc:[4026533586]'
lrwxrwxrwx. 1 root root 0 Jan  7 17:13 mnt -> 'mnt:[4026533584]'
lrwxrwxrwx. 1 root root 0 Jan  7 17:13 net -> 'net:[4026533589]'
lrwxrwxrwx. 1 root root 0 Jan  7 17:13 pid -> 'pid:[4026533587]'
lrwxrwxrwx. 1 root root 0 Jan  7 17:13 pid_for_children -> 'pid:[4026533587]'
lrwxrwxrwx. 1 root root 0 Jan  7 17:13 user -> 'user:[4026531837]'
lrwxrwxrwx. 1 root root 0 Jan  7 17:13 uts -> 'uts:[4026533585]'

```


### Connexion à un namespace spécifique (docker exec)
On peut également vérifier que l'on peut se connecter au namespace avec `nsenter`:
pour entrer dans le container (équivalent à docker exec), on utilise nsenter en y ajoutant les options (ici -a pour all) ainsi que le PID et la commande que l'on veut executer.
On fait également quelque test pour être sur que nous somme bel et bien dans le namespace/container docker avec un `ls`, `ip a` ou un `cat`
```bash
sh-4.4# nsenter -a -t 64642 /bin/sh
# ls
bin  boot  dev	etc  home  lib	lib64  media  mnt  opt	proc  root  run  sbin  srv  sys  tmp  usr  var
# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
12: eth0@if13: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:05 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.5/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
# cat /etc/debian_version
10.2
# 
```


### Namespace user
Le namespace du user est un peu différents du fonctionnement des autres namespace.
Si on veut que le namespace du user soit différent sur docker et sur la machine, nous devons spécifier cela dans le docker daemon et remapper le `/etc/subuid` et le `/etc/subgig`
```
$> ls -al /proc/$$/ns
total 0
dr-x--x--x. 2 anon anon 0 Jan  7 18:30 .
dr-xr-xr-x. 9 anon anon 0 Jan  7 18:30 ..
lrwxrwxrwx. 1 anon anon 0 Jan  7 18:30 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 18:30 ipc -> 'ipc:[4026531839]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 18:30 mnt -> 'mnt:[4026531840]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 18:30 net -> 'net:[4026531992]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 18:30 pid -> 'pid:[4026531836]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 18:30 pid_for_children -> 'pid:[4026531836]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 18:30 user -> 'user:[4026531837]'
lrwxrwxrwx. 1 anon anon 0 Jan  7 18:30 uts -> 'uts:[4026531838]'

sudo ls -al /proc/67576/ns #namespace du docker
[sudo] password for anon: 
total 0
dr-x--x--x. 2 root root 0 Jan  7 18:30 .
dr-xr-xr-x. 9 root root 0 Jan  7 18:30 ..
lrwxrwxrwx. 1 root root 0 Jan  7 18:30 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx. 1 root root 0 Jan  7 18:30 ipc -> 'ipc:[4026533075]'
lrwxrwxrwx. 1 root root 0 Jan  7 18:30 mnt -> 'mnt:[4026533073]'
lrwxrwxrwx. 1 root root 0 Jan  7 18:30 net -> 'net:[4026533078]'
lrwxrwxrwx. 1 root root 0 Jan  7 18:30 pid -> 'pid:[4026533076]'
lrwxrwxrwx. 1 root root 0 Jan  7 18:30 pid_for_children -> 'pid:[4026533076]'
lrwxrwxrwx. 1 root root 0 Jan  7 18:30 user -> 'user:[4026531837]'
lrwxrwxrwx. 1 root root 0 Jan  7 18:30 uts -> 'uts:[4026533074]'

$> grep anon /etc/passwd
anon:x:1000:1000:anon:/home/anon:/usr/bin/zsh
$> grep anon /etc/group   
anon:x:1000:
$> cat /etc/subuid  
anon:100000:65536
$> cat /etc/subgid
anon:100000:65536

```

pour changer le user du docker, on a juste a ajouter comme argument `--userns-remap`:
```bash
$> dockerd --userns-remap=root
```

changer le user du docker permet de proteger l'utilisateur face à ce genre de manipulation dangereuse:
```
docker run -v "/:/host" alpine /bin/bash
```
on évite que si quelqu'un a accès au container + sudo qu'il puisse modifier les fichiers système.


### Isolation réseaux
Nous créons une image docker avec réseaux:
```bash
$> docker run -d -p 8888:7777 debian sleep 99999
```
puis on check que l'image possède bien une IP et une carte réseaux:
```bash
$> docker inspect pedantic_gates | grep IPAddress
            "SecondaryIPAddresses": null,
            "IPAddress": "172.17.0.3",
                    "IPAddress": "172.17.0.3",
$>
$> docker exec -it pedantic_gates /bin/sh
# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
8: eth0@if9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:03 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.3/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
#
```

on peut également voir sur l'hôte que certaine règles spéciale DOCKER ont été mis en place sur le pare-feu iptables:
```bash
iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     udp  --  anywhere             anywhere             udp dpt:domain
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:domain
ACCEPT     udp  --  anywhere             anywhere             udp dpt:bootps
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:bootps

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         
DOCKER-USER  all  --  anywhere             anywhere            
DOCKER-ISOLATION-STAGE-1  all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere             ctstate RELATED,ESTABLISHED
DOCKER     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             192.168.122.0/24     ctstate RELATED,ESTABLISHED
ACCEPT     all  --  192.168.122.0/24     anywhere            
ACCEPT     all  --  anywhere             anywhere            
REJECT     all  --  anywhere             anywhere             reject-with icmp-port-unreachable
REJECT     all  --  anywhere             anywhere             reject-with icmp-port-unreachable

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     udp  --  anywhere             anywhere             udp dpt:bootpc

Chain DOCKER (1 references)
target     prot opt source               destination         
ACCEPT     tcp  --  anywhere             172.17.0.3           tcp dpt:cbt

Chain DOCKER-ISOLATION-STAGE-1 (1 references)
target     prot opt source               destination         
DOCKER-ISOLATION-STAGE-2  all  --  anywhere             anywhere            
RETURN     all  --  anywhere             anywhere            

Chain DOCKER-ISOLATION-STAGE-2 (1 references)
target     prot opt source               destination         
DROP       all  --  anywhere             anywhere            
RETURN     all  --  anywhere             anywhere            

Chain DOCKER-USER (1 references)
target     prot opt source               destination         
RETURN     all  --  anywhere             anywhere            
```


## Sandboxing Cgroups

pour acceder au cgroup d'un container, je lis l'ID du container avec docker ps et je regarde dans le dossier `/sys/fs/cgroup/<type>/FullIDcontainer/` ce qu'on y trouve, ici je regardes la mémoire max autorisé, le nombre de processus qu'il peut contenir, 
```
$> ls -la /sys/fs/cgroup/memory/docker/4911026a1821edc4b90e77f757bca88fb7729248e062477bb69f605ca6d01b64/
total 0
drwxr-xr-x. 2 root root 0 Jan 27 10:38 .
drwxr-xr-x. 3 root root 0 Jan 27 10:38 ..
-rw-r--r--. 1 root root 0 Jan 27 10:57 cgroup.clone_children
--w--w--w-. 1 root root 0 Jan 27 10:57 cgroup.event_control
-rw-r--r--. 1 root root 0 Jan 27 10:38 cgroup.procs
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.failcnt
--w-------. 1 root root 0 Jan 27 10:57 memory.force_empty
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.kmem.failcnt
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.kmem.limit_in_bytes
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.kmem.max_usage_in_bytes
-r--r--r--. 1 root root 0 Jan 27 10:57 memory.kmem.slabinfo
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.kmem.tcp.failcnt
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.kmem.tcp.limit_in_bytes
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.kmem.tcp.max_usage_in_bytes
-r--r--r--. 1 root root 0 Jan 27 10:57 memory.kmem.tcp.usage_in_bytes
-r--r--r--. 1 root root 0 Jan 27 10:57 memory.kmem.usage_in_bytes
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.limit_in_bytes
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.max_usage_in_bytes
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.memsw.failcnt
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.memsw.limit_in_bytes
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.memsw.max_usage_in_bytes
-r--r--r--. 1 root root 0 Jan 27 10:57 memory.memsw.usage_in_bytes
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.move_charge_at_immigrate
-r--r--r--. 1 root root 0 Jan 27 10:57 memory.numa_stat
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.oom_control
----------. 1 root root 0 Jan 27 10:57 memory.pressure_level
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.soft_limit_in_bytes
-r--r--r--. 1 root root 0 Jan 27 10:57 memory.stat
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.swappiness
-r--r--r--. 1 root root 0 Jan 27 10:57 memory.usage_in_bytes
-rw-r--r--. 1 root root 0 Jan 27 10:57 memory.use_hierarchy
-rw-r--r--. 1 root root 0 Jan 27 10:57 notify_on_release
-rw-r--r--. 1 root root 0 Jan 27 10:57 tasks

$> cat /sys/fs/cgroup/memory/docker/4911026a1821edc4b90e77f757bca88fb7729248e062477bb69f605ca6d01b64/memory.max_usage_in_bytes
5029888

$> cat /sys/fs/cgroup/pids/docker/4911026a1821edc4b90e77f757bca88fb7729248e062477bb69f605ca6d01b64/pids.max
max
```

ensuite, pour alterer les valeurs de la mémoire, du CPU, je vais utiliser docker run
```bash
$> docker run -d debian sleep 88888
088a47a56361c8f692e6c3ee1ee3c654d73766d6c9361431aab8fd8227faed32
$> ### ON VISUALISE LE CONTENU D'UN DOCKER PAR DEFAUT:
$>cat /sys/fs/cgroup/cpu/docker/088a47a56361c8f692e6c3ee1ee3c654d73766d6c9361431aab8fd8227faed32/cpu.shares
1024
$> cat /sys/fs/cgroup/memory/docker/088a47a56361c8f692e6c3ee1ee3c654d73766d6c9361431aab8fd8227faed32/memory.limit_in_bytes
9223372036854771712
$> cat /sys/fs/cgroup/cpu/docker/088a47a56361c8f692e6c3ee1ee3c654d73766d6c9361431aab8fd8227faed32/cpu.cfs_period_us
100000
$> cat /sys/fs/cgroup/cpu/docker/088a47a56361c8f692e6c3ee1ee3c654d73766d6c9361431aab8fd8227faed32/cpu.cfs_quota_us
-1

### ON MODIFIE LA VALEUR DU CPU PARTAGE QUI PAR DEFAUT EST DE 1024
$> docker run -d -c 2048 debian sleep 77777
23dd5ca446023a9b53008fa76b8e8451bed596289eac8483e94f9493aa469fc2
$>cat /sys/fs/cgroup/cpu/docker/23dd5ca446023a9b53008fa76b8e8451bed596289eac8483e94f9493aa469fc2/cpu.shares
2048

### ON TESTE LA MEME CHOSE AVEC LA LIMITE DE LA MEMOIRE QUI PEUT ETRE UTILISE SUR LA MACHINE
$> docker run -d -m 9999999 debian sleep 88888
1468c3ea7a87ed3a995d5edfcc92efb3c3eb9ea74506b0cd4d31b1ca6bd55537
$> cat /sys/fs/cgroup/memory/docker/1468c3ea7a87ed3a995d5edfcc92efb3c3eb9ea74506b0cd4d31b1ca6bd55537/memory.limit_in_bytes
9998336

### ON TESTE ENSUITE DE LIMITER LA CONSOMMATION POUR 1 CPU, ON OBTIENT UNE VALEUR D'EXECUTION DE 50% DU PROCESSEUR TOUTES LES 50ms:
$> docker run -d --cpu-period=50000 --cpu-quota=25000 debian sleep 77777
b73a62d556bafc7dea2e61d5c79f591f9cbce1920b5ba08c91bd22c941150342
$> cat /sys/fs/cgroup/cpu/docker/b73a62d556bafc7dea2e61d5c79f591f9cbce1920b5ba08c91bd22c941150342/cpu.cfs_period_us
50000
$> cat /sys/fs/cgroup/cpu/docker/b73a62d556bafc7dea2e61d5c79f591f9cbce1920b5ba08c91bd22c941150342/cpu.cfs_quota_us
25000
```

## Capabilities
### A la main
Ce sont les fonctionnalités de droits par rapport à des processus, le root possède à lui seul toutes les capabilities.

dans notre shell on peut voir quels sont les capabilities que nous utilisons, le current est empty:
```bash
$> capsh --print
Current: =
Bounding set =cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,cap_wake_alarm,cap_block_suspend,cap_audit_read
[...]
```

on peut également voir les capabilities en regardant dans `/proc/<pid>/status`et décoder le code hexadecimal avec capsh, ici les capabilities de tmux, un multiplexeur de terminal:
```bash
$> cat /proc/54946/status | grep Cap
CapInh: 0000000000000000
CapPrm: 0000000000000000
CapEff: 0000000000000000
CapBnd: 0000003fffffffff
CapAmb: 0000000000000000
$> capsh --decode=0000003fffffffff
0x0000003fffffffff=cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,cap_wake_alarm,cap_block_suspend,cap_audit_read
```

on peut également voir les capabilities d'un container docker comme cela:
```bash
$> docker run -d alpine sleep 99999
62e28f999df411faea55e7b7dfdce0e589da2cde65bbeb52dd7be865c7e590aa
$> systemd-cgls
Control group /:
-.slice
├─docker
│ └─62e28f999df411faea55e7b7dfdce0e589da2cde65bbeb52dd7be865c7e590aa
│   └─61075 sleep 99999
├─user.slice
[...]

$> cat /proc/61075/status | grep Cap
CapInh: 00000000a80425fb
CapPrm: 00000000a80425fb
CapEff: 00000000a80425fb
CapBnd: 00000000a80425fb
CapAmb: 0000000000000000
$> capsh --decode=00000000a80425fb
0x00000000a80425fb=cap_chown,cap_dac_override,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_net_bind_service,cap_net_raw,cap_sys_chroot,cap_mknod,cap_audit_write,cap_setfcap
$> capsh --decode=0000000000000000
0x0000000000000000=
```

on va faires des tests avec ping:
```bash
$> getcap /bin/ping
/bin/ping = cap_net_admin,cap_net_raw+p
$> sudo setcap '' /bin/ping
$> getcap /bin/ping
/bin/ping =
$> ping 172.17.0.1
ping: socket: Operation not permitted

$> strace /bin/ping 127.0.0.1
[...]
capget({version=_LINUX_CAPABILITY_VERSION_3, pid=0}, {effective=0, permitted=0, inheritable=0}) = 0
socket(AF_INET, SOCK_DGRAM, IPPROTO_ICMP) = -1 EACCES (Permission denied)
socket(AF_INET, SOCK_RAW, IPPROTO_ICMP) = -1 EPERM (Operation not permitted)
socket(AF_INET6, SOCK_DGRAM, IPPROTO_ICMPV6) = -1 EACCES (Permission denied)
socket(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6) = -1 EPERM (Operation not permitted)
openat(AT_FDCWD, "/usr/share/locale/en_US.UTF-8/LC_MESSAGES/libc.mo", O_RDONLY) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/share/locale/en_US.utf8/LC_MESSAGES/libc.mo", O_RDONLY) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/share/locale/en_US/LC_MESSAGES/libc.mo", O_RDONLY) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/share/locale/en.UTF-8/LC_MESSAGES/libc.mo", O_RDONLY) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/share/locale/en.utf8/LC_MESSAGES/libc.mo", O_RDONLY) = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/usr/share/locale/en/LC_MESSAGES/libc.mo", O_RDONLY) = -1 ENOENT (No such file or directory)
write(2, "ping: socket: Operation not perm"..., 38ping: socket: Operation not permitted
) = 38
exit_group(2)                           = ?
+++ exited with 2 +++
```

comme on le voit sur cet exemple, nous avons vérifié que ping ne pouvais plus envoyer de requetes si on lui supprimais les capabilities, et pour le strace, quoi que l'on fasse avec le ping, on aura toujours "Operation not permitted" puisque strace en lui même n'a pas de capabilities, il faut lui en attribuer pour qu'il puisse lire ce que fait le ping, le strace agis comme un sudo, un moyen d'executer une autre commande au travers d'une action.


### Avec docker

on va tester d'avoir juste le nombre limite de capabilities pour que nginx tourne:

```bash
$> docker run -d nginx
671c21859016175cf76493af9a983c315aa135667b75d776cebfaa69de4ec6ef
### PAR DEFAUT NGINX TOURNE AVEC TOUT CES CAPABILITIES:
$> pscap | grep 118505
118484 118505 root        nginx             chown, dac_override, fowner, fsetid, kill, setgid, setuid, setpcap, net_bind_service, net_raw, sys_chroot, mknod, audit_write, setfcap
$> docker stop 671c21859016175cf76493af9a983c315aa135667b75d776cebfaa69de4ec6ef 

### ON COMMENCE PAR SUPPRIMER TOUTES LES CAPABILITIES 
$> docker run --cap-drop chown --cap-drop dac_override --cap-drop fowner --cap-drop fsetid --cap-drop kill --cap-drop setgid --cap-drop setuid --cap-drop setpcap --cap-drop net_bind_service --cap-drop net_raw --cap-drop sys_chroot --cap-drop mknod --cap-drop audit_write --cap-drop setfcap -p8888:80 nginx
[...]
### APRES PLUSIEURS TEST
[...]
$> docker run --cap-drop dac_override --cap-drop fowner --cap-drop fsetid --cap-drop kill --cap-drop setpcap --cap-drop net_raw --cap-drop sys_chroot --cap-drop mknod --cap-drop audit_write --cap-drop setfcap -p8888:80 nginx
172.17.0.1 - - [27/Jan/2020:18:14:05 +0000] "GET / HTTP/1.1" 200 612 "-" "curl/7.61.1" "-"
```

au final, NGINX à besoin des capabilities `chown`, `setgid`, `setuid` et `net_bind_service` pour fonctionner.

* chown va servir au modification du propriétaire/groupe
* setgid va servir sur chown au modification du groupe
* setuid va servir sur chown au modification du proprietaire
* net_bind_service va lier un socket sur des ports < 1024 (aussi appelé port privilégiés)


Voila la fin du TP2
